﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarLib
{
    public class Calendar
    {
        string name;
        string description;
        TimeZoneInfo timeZone;
        private List<CalendarEvent> calendarEvents;



        public string Name { get => name; set => name = value; }
        public string Description { get => description; set => description = value; }
        public TimeZoneInfo TimeZone { get => timeZone; set => timeZone = value; }
        public List<CalendarEvent> CalendarEvents { get => calendarEvents; set => calendarEvents = value; }

        public Calendar()
        {
            throw new NotImplementedException();
        }
    }
}
