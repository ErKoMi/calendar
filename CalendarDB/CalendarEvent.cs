﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarLib
{

    public enum Repeat
    {
        noRepeat,
        daily,
        weekly,
        monthly,
        annually,
        weekday
    }

    public abstract class CalendarEvent
    {

        public abstract string Title { get; set; }
        public abstract DateTime TimeStart { get; set; }
        public abstract Repeat Repeat { get; set; }
        public abstract Calendar Calendar { get; set; }
        public abstract bool AllDay { get; set; }

        protected CalendarEvent(string title, DateTime timeStart, Repeat repeat, Calendar calendar, bool allDay)
        {
            Title = title;
            TimeStart = timeStart;
            Repeat = repeat;
            Calendar = calendar;
            AllDay = allDay;
        }
    }
}
