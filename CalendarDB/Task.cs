﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarLib
{
    public class Task: CalendarEvent
    {
        string title;
        string description;
        DateTime timeStart;
        Repeat repeat;
        Calendar calendar;
        bool allDay;

        public override string Title { get => title; set => title = value; }
        public override DateTime TimeStart { get => timeStart; set => timeStart = value; }
        public override Repeat Repeat { get => repeat; set => repeat = value; }
        public override Calendar Calendar { get => calendar; set => calendar = value; }
        public override bool AllDay { get => allDay; set => allDay = value; }
        public string Description { get => description; set => description = value; }

        public Task(string title, DateTime timeStart, Repeat repeat, Calendar calendar, bool allDay) 
            : base(title, timeStart, repeat, calendar, allDay)
        {
            
        }
    }
}
