﻿using System;
using System.Collections.Generic;

namespace CalendarLib
{
    public class CalendarDB
    {
        private static CalendarDB db;

        TimeZoneInfo timeZone;
        private List<Calendar> calendars = new List<Calendar>();
        private List<CalendarEvent> calendarEvents = new List<CalendarEvent>();



        public TimeZoneInfo TimeZone { get => timeZone; set => timeZone = value; }
        public List<CalendarEvent> CalendarEvents { get => calendarEvents; set => calendarEvents = value; }
        public List<Calendar> Calendars { get => calendars; set => calendars = value; }

        public static CalendarDB getCalendarDB()
        {
            if (db == null)
                return new CalendarDB();
            return db;
        }

    }
}
