﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalendarLib
{
    public class Activity: CalendarEvent
    {
        string title;
        string description;
        DateTime timeStart;
        DateTime timeEnd;
        TimeZoneInfo startTimeZone;
        TimeZoneInfo endTimeZone;
        Repeat repeat;
        Calendar calendar;
        bool allDay;
        string[] attachments;


        public override string Title { get => title; set => title = value; }
        public override DateTime TimeStart { get => timeStart; set => timeStart = value; }
        public DateTime TimeEnd { get => timeEnd; set => timeEnd = value; }
        public override Repeat Repeat { get => repeat; set => repeat = value; }
        public override Calendar Calendar { get => calendar; set => calendar = value; }
        public override bool AllDay { get => allDay; set => allDay = value; }
        public string Description { get => description; set => description = value; }
        public TimeZoneInfo StartTimeZone { get => startTimeZone; set => startTimeZone = value; }
        public TimeZoneInfo EndTimeZone { get => endTimeZone; set => endTimeZone = value; }
        public string[] Attachments { get => attachments; set => attachments = value; }


        public Activity(string title, DateTime timeStart, Repeat repeat, Calendar calendar, bool allDay) 
            : base(title, timeStart, repeat, calendar, allDay)
        {
            
        }

        static bool Attach(string path)
        {
           throw new NotImplementedException();
        }
    }


}
